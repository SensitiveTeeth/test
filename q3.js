nums1 = [1, 3]
nums2 = [2]

function median(firstArr, secondArr) {
  let result = [],
    first = 0,
    second = 0;
  while (first < firstArr.length && second < secondArr.length) {
    if (firstArr[first] < secondArr[second]) {
      result.push(firstArr[first++]);
    }
    else {
      result.push(secondArr[second++]);
    }
  }
  mergeArr = result.concat(firstArr.slice(first)).concat(secondArr.slice(second));
  if (mergeArr.length % 2 == 0) {
    return console.log(`The median is (${mergeArr[mergeArr.length / 2 - 1]} + ${mergeArr[mergeArr.length / 2]})/2 = ${(mergeArr[mergeArr.length / 2 - 1] + mergeArr[mergeArr.length / 2]) / 2}`)
  } else {
    return console.log(`The median is ${mergeArr[(Math.floor(mergeArr.length/2))]}`)
  }
}

median(nums1, nums2)
